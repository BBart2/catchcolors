﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GiftizMissions : MonoBehaviour
{

    public Sprite naked, normal, gifted, warning;

    void Start()
    {
        
        gameObject.GetComponent<Button>().onClick.AddListener(() => GiftizButtonClicked());
    }


    void Update()
    {
        ChangeGiftButtonSprite();
    }

    public void CheckMissions(int score)
    {
        if(M1_ScoreMoreThan50Points(score))
        {
            GiftizBinding.missionComplete();
            Debug.Log("mission complete!");          
        }
    }




    // score more than 50 points in one game
    bool M1_ScoreMoreThan50Points(int score)
    {
        int mission1Score = 50;

        if (score > mission1Score)
            return true;
        else
            return false;

    }

    
    public void ChangeGiftButtonSprite()
    {
        SpriteRenderer sp = gameObject.GetComponent<SpriteRenderer>();

        switch(GiftizBinding.giftizButtonState)
        {
            case GiftizBinding.GiftizButtonState.Invisible: sp.sprite = naked; break;

            case GiftizBinding.GiftizButtonState.Naked: sp.sprite = normal; break;

            case GiftizBinding.GiftizButtonState.Badge: sp.sprite = gifted; break;

            case GiftizBinding.GiftizButtonState.Warning: sp.sprite = warning; break;
        }
    }

    void GiftizButtonClicked()
    {
       
        GiftizBinding.buttonClicked();
    }
}
