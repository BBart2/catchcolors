﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Advertisements;

public class AdManager : MonoBehaviour
{
    [SerializeField]
    string gameID = "1025899";
    GameManager gmScript;
    Text afterPlayAdText;
    SaveAndLoadColors saveAndLoadScript;
    GameOverManager gameOverMg;
    int rewardValue = 1000;
    void Awake()
    {
        gmScript = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        saveAndLoadScript = GameObject.FindGameObjectWithTag("Save&Load").GetComponent<SaveAndLoadColors>();
        gameOverMg = GameObject.FindGameObjectWithTag("HUD").GetComponent<GameOverManager>();

        //adbudd!

        AdBuddizBinding.SetLogLevel(AdBuddizBinding.ABLogLevel.Info);         // log level
        AdBuddizBinding.SetAndroidPublisherKey("e18e13f5-c13c-4464-a5fc-f09d4297ce10");
       // AdBuddizBinding.SetTestModeActive();                                  // to delete before submitting to store
        AdBuddizBinding.CacheAds();
        AdBuddizBinding.RewardedVideo.Fetch();

        //    Advertisement.Initialize(gameID, true);
    }
    //unity ads
 /*   public void ShowAd(string zone = "")
    {
#if UNITY_EDITOR
        StartCoroutine(WaitForAd());
#endif

        if (string.Equals(zone, ""))
            zone = null;

        ShowOptions options = new ShowOptions();
        options.resultCallback = AdCallbackhandler;

        if (Advertisement.IsReady(zone))
            Advertisement.Show(zone, options);
    }

    void AdCallbackhandler(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                // GameObject rewardAdText_GO = GameObject.FindGameObjectWithTag("RewardAdText");
                // GameObject adText = GameObject.FindGameObjectWithTag("AdText");
                //Color adColor = Color.black;
                //  Color oldAdcl = Color.black;

                //oldAdcl.a = 0f;          
                //adText.GetComponent<Text>().color = oldAdcl;
                // adText.GetComponent<Text>().text = "";

                // adColor.a = 1f;
                // rewardAdText_GO.GetComponent<Text>().color = adColor;
                // rewardAdText_GO.GetComponent<Text>().text ="+" + rewardValue.ToString()  +  "$";
                gmScript.moneyValue += rewardValue;
                gameOverMg.TurnOffAdButton();
                // gameOverMg.moneyText.text = gmScript.moneyValue.ToString() + "$";

                //save ad watched time
                gmScript.lastAdWatchedTime = System.DateTime.Now;

                saveAndLoadScript.Save();

                // Debug.Log("Ad Finished. Rewarding player...");
                break;
            case ShowResult.Skipped:
                // Debug.Log("Ad skipped. Son, I am dissapointed in you");
                break;
            case ShowResult.Failed:
                // Debug.Log("I swear this has never happened to me before");
                break;
        }
    }
    IEnumerator WaitForAd()
    {
        float currentTimeScale = Time.timeScale;
        Time.timeScale = 0f;
        yield return null;

        while (Advertisement.isShowing)
            yield return null;

        Time.timeScale = currentTimeScale;
    }
    */
    //AdBuddiz

    void OnEnable()
    {
        // Listen to AdBuddiz events
        AdBuddizManager.didShowAd += DidShowAd;
        AdBuddizManager.didClick += DidClick;
        AdBuddizManager.didHideAd += DidHideAd;

        //Listen to AdBuddiz events for incentivized video
        AdBuddizRewardedVideoManager.didFetch += DidFetch;
        AdBuddizRewardedVideoManager.didFail += DidFail;
        AdBuddizRewardedVideoManager.didComplete += DidComplete;
        AdBuddizRewardedVideoManager.didNotComplete += DidNotComplete;
    }

    void OnDisable()
    {
        // Remove all event handlers      
        AdBuddizManager.didShowAd -= DidShowAd;
        AdBuddizManager.didClick -= DidClick;
        AdBuddizManager.didHideAd -= DidHideAd;

        AdBuddizRewardedVideoManager.didFetch -= DidFetch;
        AdBuddizRewardedVideoManager.didFail -= DidFail;
        AdBuddizRewardedVideoManager.didComplete -= DidComplete;
        AdBuddizRewardedVideoManager.didNotComplete -= DidNotComplete;
    }


    void DidShowAd()
    {
       // AdBuddizBinding.LogNative("DidShowAd");
        //AdBuddizBinding.ToastNative("DidShowAd");
        Debug.Log("Unity: DidShowAd");
    }

    void DidClick()
    {
      //  AdBuddizBinding.LogNative("DidClick");
       // AdBuddizBinding.ToastNative("DidClick");
        Debug.Log("Unity: DidClick");
    }

    void DidHideAd()
    {
        //AdBuddizBinding.LogNative("DidHideAd");
        //AdBuddizBinding.ToastNative("DidHideAd");
        Debug.Log("Unity: DidHideAd");
    }

    void DidFetch()
    {
       // AdBuddizBinding.LogNative("DidFetch");
       // AdBuddizBinding.ToastNative("DidFetch");
        Debug.Log("Unity: DidFetch");
    }

    void DidFail(string adBuddizError)
    {
      //  AdBuddizBinding.LogNative("DidFail: " + adBuddizError);
       // AdBuddizBinding.ToastNative("DidFail: " + adBuddizError);
        Debug.Log("Unity: DidFail: " + adBuddizError);
    }

    //adbudd REWARDED COMPLETED
    void DidComplete()
    {
       // AdBuddizBinding.LogNative("DidComplete");
       // AdBuddizBinding.ToastNative("DidComplete");
        Debug.Log("Unity: DidComplete");


        Debug.Log("dodlame reward za reklame!");
        gmScript.moneyValue += rewardValue;
        gameOverMg.TurnOffAdButton();

        //save ad watched time
        gmScript.lastAdWatchedTime = System.DateTime.Now;

        saveAndLoadScript.Save();

    }

    void DidNotComplete()
    {
       // AdBuddizBinding.LogNative("DidNotComplete");
       // AdBuddizBinding.ToastNative("DidNotComplete");
        Debug.Log("Unity: DidNotComplete");
    }

    public void ShowBuddizRewardAd()
    {
        AdBuddizBinding.RewardedVideo.Show();
        Debug.Log("showing adbudd!");
    }

}

