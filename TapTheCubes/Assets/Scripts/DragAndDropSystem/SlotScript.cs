﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class SlotScript : MonoBehaviour , IDropHandler
{
    public Color currentColor;
    public GameManager gmScript;
    ShopManager shopManagerScript;

    void Start()
    {
        currentColor = GetComponent<Image>().color;
        gmScript = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        shopManagerScript = GameObject.FindGameObjectWithTag("HUD").GetComponent<ShopManager>();
    }
    public GameObject item
    {
        get
        {
            if(transform.childCount > 0)
            {
                return transform.GetChild(0).gameObject;
            }
            return null;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        GameObject dragingItem;

        if(!item)
        {
            dragingItem = MyDragHandler.itemBeingDragged;                // dragingItem = DragHandler.itemBeingDragged;
            currentColor = dragingItem.GetComponent<Image>().color;       // zmiana koloru na kolor przeciagany
                                                                          // a pozniej przeslanie do tablicy glownej colorow

           // Debug.Log(gameObject.name);


            if (GameManager.CheckIfCurrentColorsIsInUse(gmScript.buttonsColorsArray, currentColor) || gmScript.bonusColor == currentColor)
            {
                Debug.Log("cant change!");
                gmScript.badClickAudio.SetActive(false);
                gmScript.badClickAudio.SetActive(true);
            }

            else
            {
                
                GetComponent<Image>().color = currentColor;                   

                switch (gameObject.name)
                {
                    case "Slot 0":
                        gmScript.buttonsColorsArray[0] = currentColor;
                        break;

                    case "Slot 1":
                        gmScript.buttonsColorsArray[1] = currentColor;
                        break;

                    case "Slot 2":
                        gmScript.buttonsColorsArray[2] = currentColor;
                        break;

                    case "Slot 3":
                        gmScript.buttonsColorsArray[3] = currentColor;
                        break;

                    case "Slot 4":
                        gmScript.buttonsColorsArray[4] = currentColor;
                        break;

                    case "Slot 5":
                        gmScript.buttonsColorsArray[5] = currentColor;
                        break;

                    case "ChildrenBonusPanel":
                        gmScript.bonusColor = currentColor;
                        break;

                }
            }

            //GameManager.SendGameObejctColorsToMainArrayColors(shopManagerScript.currentColorsInUseArr, gmScript.buttonsColorsArray);
            
            MyDragHandler.itemBeingDragged.transform.SetParent(transform);
            ExecuteEvents.ExecuteHierarchy<IHasChanged>(gameObject, null, (x, y) => x.HasChanged());
        }
    }
}
