﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class Dropdown : MonoBehaviour, IPointerDownHandler //, IBeginDragHandler , IDragHandler, IEndDragHandler  //, IPointerEnterHandler //, IPointerExitHandler
{
    //Dropdown core variables
    public List<DropdownChild> children;
    public RectTransform container;
    public bool isOpen;

    //Main Button & Children properties
    public bool dropShadow = false;
    public float subHeight = 20;
    public int subFontSize = 11;
    public Color normal = Color.white, highlight = Color.red, press = Color.gray;

    //Dropdown accessors
    public Text mainText { get { return transform.FindChild("Text").GetComponent<Text>(); } }
    public Image image { get { return GetComponent<Image>(); } }


    // Use this for initialization
    public void Start()
    {
        isOpen = false;
    }
    public void Update()
    {
        Vector3 scale = container.localScale;

        scale.y = Mathf.Lerp(scale.y, isOpen ? 1 : 0, Time.deltaTime * 12);
        container.localScale = scale;
    }

    //public void OnPointerEnter(PointerEventData eventData)
    //{
    //    isOpen = true;
    //}

    //public void OnPointerExit(PointerEventData eventData)
    //{
    //    isOpen = false;
    //}

   
    public void OnPointerDown(PointerEventData eventData)
    {
        isOpen = !isOpen;
    }
    
}


[System.Serializable]
public class DropdownChild
{
    //Object access
    public RectTransform btnRect;

    //Outside access
    public Text buttonText;
    public Button.ButtonClickedEvent buttonEvents;

    //Inside access
    private LayoutElement element { get { return btnRect.GetComponent<LayoutElement>(); } }
    private Image image { get { return btnRect.GetComponent<Image>(); } }
    private Button button { get { return btnRect.GetComponent<Button>(); } }

    public DropdownChild(RectTransform parent)
    {
        btnRect = UIUtility.NewButton("Child", "Sub Button", parent).GetComponent<RectTransform>();
        btnRect.gameObject.AddComponent<LayoutElement>();

        buttonText = btnRect.transform.FindChild("Text").GetComponent<Text>();
        buttonEvents = button.onClick;
    }
    public bool UpdateChild(Dropdown parent)
    {
        if (btnRect == null || btnRect.gameObject == null)
            return false;

        element.minHeight = parent.subHeight;

        image.sprite = parent.image.sprite;
        image.type = parent.image.type;

        ColorBlock b = button.colors;
        b.normalColor = parent.normal;
        b.highlightedColor = parent.highlight;
        b.pressedColor = parent.press;
        button.colors = b;

        buttonText.fontSize = parent.subFontSize;
        buttonText.color = parent.mainText.color;
        if (parent.dropShadow)
        {
            if (buttonText.GetComponent<Shadow>() == null)
                buttonText.gameObject.AddComponent<Shadow>();
        }
        else
        {
            if (buttonText.GetComponent<Shadow>() != null)
                GameObject.DestroyImmediate(buttonText.GetComponent<Shadow>());
        }

        button.onClick = buttonEvents;
        return true;
    }
}