﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class LifeManagerV2 : MonoBehaviour
{

    public GameObject startImagepos;
    public GameObject image;
    public GameObject[] LifesnArr;
    public static int lifeNumber;
    //public GameObject parrentPanel;
    Vector2 imagePos;
   // public ButtonSpawner BtScript;





    // Use this for initialization
    void Start()
    {
        lifeNumber = 3;
        LifesnArr = new GameObject[lifeNumber];
        LifesInitialization(lifeNumber);

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.A))
        {
            lifeNumber--;
            DecressLife(lifeNumber);
        }
    }


    void LifesInitialization(int lifenumber)
    {
        int index = 0;
        GameObject currentImage;
        float startXpos = startImagepos.transform.position.x - 50;
        float startYpos = startImagepos.transform.position.y;
        imagePos = new Vector2(startXpos, startYpos);

        for (int j = 0; j < lifeNumber; j++)
        {
            imagePos = new Vector2(imagePos.x + 50, imagePos.y);
            currentImage = (GameObject)Instantiate(image, imagePos, Quaternion.identity);
            currentImage.transform.parent = transform;

           // currentImage.GetComponent<Image>().color = BtScript.ChooseRandomColor(BtScript.buttonsColors);
            //currentImage.tag = "EmptyButton";
            LifesnArr[index] = currentImage;
            index++;
        }
    }

   public  void DecressLife(int lifeNmbr)
    {
        GameObject tempLifeImg = LifesnArr[lifeNmbr];
        Color tempColor = tempLifeImg.GetComponent<Image>().color;
        tempColor = new Color(tempColor.r, tempColor.g, tempColor.b, 0);
        tempLifeImg.GetComponent<Image>().color = tempColor;
    }

}
