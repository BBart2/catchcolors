﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BonusTimeManager : MonoBehaviour {

    public static float time;


    Text text;


    void Awake()
    {
        text = GetComponent<Text>();
    }


    void Update()
    {
        if (time >= 0)
        {
            time -= Time.deltaTime;
            text.text = "Bonus Time : " + (int)time;
        }
        //else
        //{
        //    Debug.Log("GameOver!");

        //}
    }
}