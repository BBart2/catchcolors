﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIUtility : MonoBehaviour
{
#if UNITY_EDITOR
    [MenuItem("GameObject/UI/Dropdown")]
    public static void CreateDropdown()
    {
        NewButton("Dropdown Button", "Dropdown Menu", DetermineParent()).gameObject.AddComponent<Dropdown>();
    }
#endif

    public static RectTransform DetermineParent()
    {
        Canvas c = FindObjectOfType<Canvas>();

        if (c == null)
        {
            c = new GameObject().AddComponent<Canvas>();
            c.name = "Canvas";
            c.renderMode = RenderMode.ScreenSpaceOverlay;
            c.gameObject.layer = 5;
            c.gameObject.AddComponent<CanvasScaler>();
            c.gameObject.AddComponent<GraphicRaycaster>();

            if (FindObjectOfType<EventSystem>() == null)
            {
                EventSystem e = new GameObject().AddComponent<EventSystem>();
                e.name = "Event System";
                e.gameObject.AddComponent<StandaloneInputModule>();
                e.gameObject.AddComponent<TouchInputModule>();
            }
        }

        return c.GetComponent<RectTransform>();
    }

    public static RectTransform NewUIElement(string name, RectTransform parent)
    {
        RectTransform temp = new GameObject().AddComponent<RectTransform>();
        temp.name = name;
        temp.gameObject.layer = 5;
        temp.SetParent(parent);
        temp.localScale = new Vector3(1, 1, 1);
        temp.localPosition = new Vector3(0, 0, 0);

        return temp;
    }

    public static Button NewButton(string name, string buttonText, RectTransform parent)
    {
        RectTransform btnRect = NewUIElement(name, parent);
        btnRect.gameObject.AddComponent<Image>();
        btnRect.gameObject.AddComponent<Button>();
        ScaleRect(btnRect, 160, 30);
        NewText(buttonText, btnRect);
        return btnRect.gameObject.GetComponent<Button>();
    }

    public static Text NewText(string text, RectTransform parent)
    {
        Text t = NewUIElement("Text", parent).gameObject.AddComponent<Text>();
        t.text = text;
        t.alignment = TextAnchor.MiddleCenter;
        t.color = Color.black;

        //Stretch anchors
        ScaleRect(t.GetComponent<RectTransform>(), 0, 0);
        t.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
        t.GetComponent<RectTransform>().anchorMax = new Vector2(1, 1);
        return t;
    }

    public static void ScaleRect(RectTransform r, float w, float h)
    {
        r.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, w);
        r.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, h);
    }
    public static void PositionRect(RectTransform r, float x, float y)
    {
        r.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, x, r.rect.width);
        r.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, y, r.rect.height);
    }
}
