﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class GeneratePattern : MonoBehaviour {

    public GameObject startImagepos;
    public GameObject image;
    public GameObject[] imagePatternArr = new GameObject[4];
    //public GameObject parrentPanel;
    Vector2 imagePos;
   public GameManager GmManagScript;
    bool gamestarted = false;
    



	// Use this for initialization
	void Start ()
    {
        
        //imagePatternArr = new GameObject[4];
       
           
	}
	
	//// Update is called once per frame
	//void Update ()
 //   {
	//    if(GmManagScript.globalGameStart && !gamestarted)
 //       {
 //           PatternInitialization();
 //           gamestarted = true;
 //       }
	//}


   public void PatternInitialization()
    {
        int index = 0;
        GameObject currentImage;
        float startXpos = startImagepos.transform.position.x + 8 ;
        float startYpos = startImagepos.transform.position.y ;
        imagePos = new Vector2(startXpos, startYpos);

        for (int j = 0; j < 4; j++)
        {
            imagePos = new Vector2(imagePos.x + 35, imagePos.y);
            // currentImage = (GameObject)Instantiate(image, imagePos, Quaternion.identity);
            currentImage = (GameObject)Instantiate(image);
            currentImage.transform.parent = transform;
            RectTransform rectTr = currentImage.GetComponent<RectTransform>();
            rectTr.anchoredPosition = new Vector3(10f + (j*70),0f,0f);
            currentImage.GetComponent<RectTransform>().localScale = new Vector3(2f, 2f, 2f);

            currentImage.GetComponent<Image>().color = GmManagScript.ChooseRandomColor(GmManagScript.buttonsColorsArray);
            //currentImage.tag = "EmptyButton";
            imagePatternArr[index] = currentImage;
            index++;
        }
    }
    
}
