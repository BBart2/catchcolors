﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class Inventory : MonoBehaviour , IHasChanged
{
    [SerializeField]
    Transform slots;
    [SerializeField]
    Text inventoryText;

    public Color newColorDragged;

    public GameManager GameMangerScript;

    void Start()
    {
        GameMangerScript = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        HasChanged();
    }

    public void HasChanged()
    {
        //System.Text.StringBuilder builder = new System.Text.StringBuilder();
        //builder.Append(" - ");
        foreach (Transform slotTransform in slots)
        {
            GameObject item = slotTransform.GetComponent<SlotScript>().item;
            if(item)
            {
                //builder.Append(item.name);
                //builder.Append(" - ");
                newColorDragged = item.GetComponent<Image>().color;
                //Debug.Log("dragged COLOR!");
               // Debug.Log(item.name);
            }
        }
        //inventoryText.text = builder.ToString();
    }

   


}

namespace UnityEngine.EventSystems
{
    public interface IHasChanged : IEventSystemHandler
    {
        void HasChanged();
    }
}