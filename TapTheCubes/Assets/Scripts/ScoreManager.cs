﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    GameManager gmScript;
    GameOverManager gameOverMGScript;
    

    public static int score;
    public int highScore;
    public int thatGameMoney;
    bool moneyAdd = false;
    bool panelSpawned = false;

    Text text;


    void Awake ()
    {        
        gmScript = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        gameOverMGScript = GameObject.FindGameObjectWithTag("HUD").GetComponent<GameOverManager>();

        text = GetComponent <Text> ();
        score = 0;
        //PlayerPrefs.GetInt("highScorePref");
        highScore = PlayerPrefs.GetInt("highScorePref");
        // money = PlayerPrefs.GetInt("moneyPref");
      //  money = gmScript.moneyValue;
    }


    void Update ()
    {
        text.text =  score.ToString();

        if(score > highScore && !gmScript.tutorialTime)
        {
            PlayerPrefs.SetInt("highScorePref", score);
            highScore = score;
            gameOverMGScript.newHighscore = true;
        }

        if(GameTimeMaganer.time <= 0f && !moneyAdd)
        {
                  
            gmScript.moneyValue += thatGameMoney ;
            gameOverMGScript.score = score; 
            //PlayerPrefs.SetInt("moneyPref", money);
            moneyAdd = true;
            gameOverMGScript.SpawnGameOverPanel();
        }
        
    }
}
