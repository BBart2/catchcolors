﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;

[CustomEditor(typeof(Dropdown))]
public class DropdownEditor : Editor
{
    Dropdown currDropdown;
    bool showChildren;
    SerializedProperty serializedChildren;

    void OnEnable()
    {
        currDropdown = (Dropdown)target;
        serializedChildren = serializedObject.FindProperty("children");
    }

    public override void OnInspectorGUI()
    {
        UpdateContainer();

        if (GUILayout.Button("Add Child"))
        {
            AddChild();
        }
        currDropdown.isOpen = EditorGUILayout.Toggle("Toggle", currDropdown.isOpen);

        currDropdown.mainText.text = EditorGUILayout.TextField("Text", currDropdown.mainText.text);
        currDropdown.mainText.fontSize = EditorGUILayout.IntField("Font Size", currDropdown.mainText.fontSize);
        GUILayout.Label("");
        currDropdown.mainText.font = (Font)EditorGUILayout.ObjectField("Font", currDropdown.mainText.font, typeof(Font), false);
        currDropdown.mainText.color = EditorGUILayout.ColorField("Font Color", currDropdown.mainText.color);
        currDropdown.dropShadow = EditorGUILayout.Toggle("Drop Shadow", currDropdown.dropShadow);
        GUILayout.Label("");
        currDropdown.image.sprite = (Sprite)EditorGUILayout.ObjectField("Button Sprite", currDropdown.image.sprite, typeof(Sprite), false, GUILayout.Height(16));
        currDropdown.image.type = (Image.Type)EditorGUILayout.EnumPopup("Type", currDropdown.image.type);
        currDropdown.normal = EditorGUILayout.ColorField("Normal", currDropdown.normal);
        currDropdown.image.color = currDropdown.normal;
        currDropdown.highlight = EditorGUILayout.ColorField("Hover", currDropdown.highlight);
        currDropdown.press = EditorGUILayout.ColorField("Press", currDropdown.press);

        GUILayout.Space(10);
        GUILayout.Label("Children", EditorStyles.centeredGreyMiniLabel);
        currDropdown.subFontSize = EditorGUILayout.IntField("Font Size", currDropdown.subFontSize);
        currDropdown.subHeight = EditorGUILayout.FloatField("Height", currDropdown.subHeight);
        showChildren = EditorGUILayout.Foldout(showChildren, "Edit children");
        if (showChildren)
        {
            for (int i = 0; i < currDropdown.children.Count; i++)
            {
                GUILayout.Label("Element " + i.ToString(), EditorStyles.centeredGreyMiniLabel);
                currDropdown.children[i].btnRect.name = EditorGUILayout.TextField("Name", currDropdown.children[i].btnRect.name);
                currDropdown.children[i].buttonText.text = EditorGUILayout.TextField("Button Text", currDropdown.children[i].buttonText.text);
                GUILayout.BeginHorizontal();
                GUILayout.Label("");
                if (GUILayout.Button("Remove", EditorStyles.miniButton))
                {
                    DestroyImmediate(currDropdown.children[i].btnRect.gameObject);
                }
                GUILayout.EndHorizontal();
                serializedObject.UpdateIfDirtyOrScript();
                EditorGUILayout.PropertyField(serializedChildren.GetArrayElementAtIndex(i).FindPropertyRelative("buttonEvents"));
                serializedObject.ApplyModifiedProperties();
            }
        }

        UpdateDropdown();
        EditorUtility.SetDirty(currDropdown);
        Repaint();
    }




    void AddChild()
    {
        if (currDropdown.children == null)
            currDropdown.children = new System.Collections.Generic.List<DropdownChild>();
        currDropdown.children.Add(new DropdownChild(currDropdown.container));
    }
    void UpdateDropdown()
    {
        if (currDropdown.dropShadow)
        {
            if (currDropdown.mainText.GetComponent<Shadow>() == null)
                currDropdown.mainText.gameObject.AddComponent<Shadow>();
        }
        else
        {
            if (currDropdown.mainText.GetComponent<Shadow>() != null)
                DestroyImmediate(currDropdown.mainText.GetComponent<Shadow>());
        }
        if (currDropdown.children != null && currDropdown.children.Count > 0)
        {
            for (int i = 0; i < currDropdown.children.Count; i++)
            {
                if (!currDropdown.children[i].UpdateChild(currDropdown))
                {
                    currDropdown.children.RemoveAt(i);
                }
            }
        }
    }
    void UpdateContainer()
    {
        if (currDropdown.container == null)
        {
            if (currDropdown.transform.FindChild("Container") == null)
                currDropdown.container = UIUtility.NewUIElement("Container", currDropdown.GetComponent<RectTransform>());
            else
                currDropdown.container = currDropdown.transform.FindChild("Container").GetComponent<RectTransform>();

            currDropdown.container.gameObject.AddComponent<VerticalLayoutGroup>();
            //Bottom stretch
            UIUtility.ScaleRect(currDropdown.container, 0, 0);
            currDropdown.container.anchorMax = new Vector2(1, 0);
            currDropdown.container.anchorMin = new Vector2(0, 0);
        }

        currDropdown.Update();
    }
}
