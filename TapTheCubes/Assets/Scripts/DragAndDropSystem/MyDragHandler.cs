﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class MyDragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public DragColorManager dragClrMangScript;
    ShopManager shopManagerScript;

    public static GameObject itemBeingDragged;
    Vector3 startPosition;
    Transform startParent;
    Transform tempParent;  // for MASK
   public Transform resizePanel;  // resize sprite to begining size

    //Transform startPanelForMask;

    void Start()
    {
        tempParent = GameObject.FindGameObjectWithTag("TempMaskParent").transform;  // MASK
        dragClrMangScript = GetComponentInParent<DragColorManager>();
        shopManagerScript = GameObject.FindGameObjectWithTag("HUD").GetComponent<ShopManager>();
       // startPanelForMask = GameObject.FindGameObjectWithTag("StartPanelForMask").transform;
        resizePanel = GameObject.FindGameObjectWithTag("ResizePanel").transform;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        itemBeingDragged = gameObject;
        startPosition = transform.position;
        startParent = transform.parent;
        transform.parent = tempParent;                    //transform.parent.SetParent(tempParent); 
        GetComponent<CanvasGroup>().blocksRaycasts = false;
        
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        GameObject tempgo = itemBeingDragged;
        
        transform.parent = resizePanel;

        itemBeingDragged = null;
        GetComponent<CanvasGroup>().blocksRaycasts = true;

        StartCoroutine("ParentToPrimaryParent");
        //if (transform.parent == resizePanel)
        ////{
        //    transform.parent = startParent;
        //    transform.position = startPosition;
        //}

        // dragClrMangScript.SettingOrderInPanel(tempgo.transform.parent.transform.gameObject, startPanelForMask);

    }

    IEnumerator ParentToPrimaryParent()
    {
        //Debug.Log("corutine");

        //Debug.Log("just before");
        yield return new WaitForSeconds(.1f);

        transform.parent = startParent;
        transform.position = startPosition;
        //Debug.Log("end corutine");
    }
    
}
