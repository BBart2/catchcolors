﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShopShapeButtonManager : MonoBehaviour {

    SaveAndLoadColors saveAndLoadScript;
    ShopManager shopManagerScript;
    public GameManager gmScript;

    public GameObject basicShapeButton;

    public GameObject basicShapePanel;
    

    public GameObject[] go_Shapes = new GameObject[16]; //22

    public GameObject[] shapePanels = new GameObject[16];

    public GameObject[] boughtShapes = new GameObject[16];
    
    //shape in use its a panel with one button as parrent and second child also buton
    public GameObject currentShapeInUse;
    //text of a use button
    Text currentShapeInUseTextComponent;
    //current set button
    Button currentShapeSetButton;
    //current set shape go
    GameObject go_currentShapeSet;

    string st_textOfShapeInUse = "Set!";
    string st_StandardTextOfSetting = "Use!";

    Color colorOfNowUsingShape = Color.green;

    // does shopshapebutton already start?
    bool shopShapeButtonStart = false;
    
    // text component which has number of shape which is in use
    //public Text Textnumber;
    //public string numberInUse = "";

    public string[] nameOfBoughtShapes = new string[17];

    public Sprite[] sprite_Shapes = new Sprite[16];

    int moneyValue;
    int priceForOneShape = 7000;

    // money text in shop 
    public Text moneyText;

    Image[] basicImage ;    
    public Sprite basicSprite;

    //normal or bonus set panel, did Bonus taped?
    bool bonusTaped;

   // float dimScreen;
    void Start ()
    {       

        moneyValue = gmScript.moneyValue;

        shopManagerScript = GameObject.FindGameObjectWithTag("HUD").GetComponent<ShopManager>();
        saveAndLoadScript = GameObject.Find("__Save&LoadManager").GetComponent<SaveAndLoadColors>();

        shopShapeButtonStart = true; 
       // 

        // basicSprite = Resources.Load<Sprite>("UI/BasicCircle");

        //sprite_Shapes = Resources.LoadAll<Sprite>("UI/Shapes");
      //  SpriteLoad();
       // StartInitialization();
        //        Firstinitialization();

        //loading bought colors etc
        saveAndLoadScript.Load();
        //numberInUse = gmScript.GlobalNumberInUse;
        saveAndLoadScript.LoadBoughtShapeArray(nameOfBoughtShapes, boughtShapes, shapePanels);
        

        //disabling buy button and switch to set if bought already
        DisableBuyButtonAndActiveSetButton();
        //setting shape to set earlier
        CheckWhichShapeIsInUseAndSet(shopShapeButtonStart);

        if(currentShapeInUse == null)
        {
            currentShapeInUse = go_Shapes[1];
        }
        
        //Getting basic sprite
        basicImage = basicShapeButton.GetComponentsInChildren<Image>(true);
        basicSprite = basicImage[1].sprite;
    }
	
    void SpriteLoad()
    {
        sprite_Shapes[0] = gmScript.basicButton.GetComponent<Image>().sprite;

        for (int i = 1; i < sprite_Shapes.Length; i++)
        {
            string UIpath = "UI/Shapes/Symbol";

            string number = i.ToString();
            //Debug.Log(number);
            UIpath += number;

           // Debug.Log(UIpath);
            sprite_Shapes[i] = Resources.Load<Sprite>(UIpath);
          

        }
    }


    public void StartInitialization()
    {
        SpriteLoad();

        for (int i = 0; i < go_Shapes.Length; i++)
        {
            string buttonName = "OneShapePanel";
            string prefix = "_";
            GameObject tempShapeGO = null;
            GameObject tempShapePanel;

            // buy & set button
            Button buyShapeButton, setShapeButton;
            Image[] tempChildrenImage;
            // Sprite tempChildrenSprite;
            // tempShapeGO.GetComponent<Image>().sprite = basicSprite;
            tempShapePanel = (GameObject)Instantiate(basicShapePanel);
            //tempShapeGO.transform.parent = transform;
            tempShapePanel.transform.SetParent(transform);
            tempShapePanel.transform.localScale = new Vector3(1f, 1f, 1f);

            int children = tempShapePanel.transform.childCount;
            GameObject[] tempShapePanelChildren = new GameObject[children];
            for (int j = 0; j < children; j++)
            {
                Transform tempChildrenTransform;
                tempChildrenTransform = tempShapePanel.transform.GetChild(j);

                if(tempChildrenTransform.tag == "OneShopShapeButton")
                {
                   // Debug.Log("znalazlem!");
                    tempShapeGO = tempChildrenTransform.gameObject;
                }

                if(tempChildrenTransform.tag == "BuyShapeButton")
                {
                    buyShapeButton = tempChildrenTransform.gameObject.GetComponent<Button>();
                    AddListener(buyShapeButton, tempChildrenTransform.tag);
                }

                if (tempChildrenTransform.tag == "SetShapeButton")
                {
                    setShapeButton = tempChildrenTransform.gameObject.GetComponent<Button>();
                    AddListener(setShapeButton, tempChildrenTransform.tag);
                }

            }


            //creating a button background(changing children sprite)
            tempShapeGO.GetComponent<Image>().sprite = sprite_Shapes[i];
            tempChildrenImage = tempShapeGO.GetComponentsInChildren<Image>(true);
            tempChildrenImage[1].sprite = sprite_Shapes[i];

            string tempString = "";
                
            //setting Shape panel name to ex 0_OneShapePanel , 12_OneShapePanel....             
                tempString += i.ToString();
                tempString += prefix;
                tempString += buttonName;
            

            //// nameing button 1,2,3...
            //buttonName += i.ToString();

            tempShapePanel.name = tempString;


            //On Click event!
           // tempShapeGO.GetComponent<Button>().onClick.AddListener(() => ButtonClicked(tempShapeGO.GetComponent<Button>()));

            go_Shapes[i] = tempShapeGO;
            shapePanels[i] = tempShapePanel;
        }
    }

    //before panels just shapes
    void Firstinitialization()
    {
        //for (int i = 0; i < sprite_Shapes.Length; i++)
        //{
        //    //sprite_Shapes[i] = 
        //}

        for (int i = 0; i < go_Shapes.Length; i++)
        {
            string buttonName = "ShapeButton";
            GameObject tempShapeGO;
            Image[] tempChildrenImage;
           // Sprite tempChildrenSprite;
            // tempShapeGO.GetComponent<Image>().sprite = basicSprite;
            tempShapeGO = (GameObject)Instantiate(basicShapeButton);
            //tempShapeGO.transform.parent = transform;
            tempShapeGO.transform.SetParent(transform);

            //creating a button background(changing children sprite)
            tempShapeGO.GetComponent<Image>().sprite = sprite_Shapes[i];
            tempChildrenImage = tempShapeGO.GetComponentsInChildren<Image>(true);
            tempChildrenImage[1].sprite = sprite_Shapes[i];


            // nameing button 1,2,3...
            buttonName += i.ToString();
            tempShapeGO.name = buttonName;


            //On Click event!
            tempShapeGO.GetComponent<Button>().onClick.AddListener(() => ButtonClicked(tempShapeGO.GetComponent<Button>()));

            go_Shapes[i] = tempShapeGO;
        }
    }


    void ButtonClicked(Button bt)
    {
      //  Debug.Log("Clicked " + bt.gameObject.name);
    }


    //LISTENERS
    void AddListener(Button b,string tag)
    {
        if (tag == "BuyShapeButton")
        {
            b.onClick.AddListener(() => BuyShapeButtonTapped(b));
        }
        else if(tag == "SetShapeButton")
        {
            b.onClick.AddListener(() => SetShapeButtonTapped(b));
        }
    }

    void BuyShapeButtonTapped(Button bt)
    {
        if (moneyValue < priceForOneShape)
        {
           // Debug.Log("Go and earn more money!");
            gmScript.badClickAudio.SetActive(false);
            gmScript.badClickAudio.SetActive(true);
        }
        else
        {

            //buying button and adding to bought array button
            int number = 0;
            GameObject parentGameObject;
            parentGameObject = bt.transform.parent.gameObject;
            number = DragColorManager.GettingNumberFromString(parentGameObject.name);
            boughtShapes[number] = parentGameObject;
            gmScript.moneyValue -= priceForOneShape;
            //setting moneyValue to prefab
            //PlayerPrefs.SetInt("moneyPref", moneyValue);
            moneyText.text =  gmScript.moneyValue.ToString() ;

            //get all text component from parent gameobject
            Transform[] tempChildrenGO = new Transform[3];

            tempChildrenGO = parentGameObject.GetComponentsInChildren<Transform>(true);

            //search the specyfic text gameobject(PRICE text) 
            GameObject thePriceGO = null;
            GameObject buyButton = null;
            GameObject setButton = null;

            for (int i = 0; i < tempChildrenGO.Length; i++)
            {
                if (tempChildrenGO[i].tag == "ShapePrice")
                {
                    thePriceGO = tempChildrenGO[i].gameObject;
                }
                if (tempChildrenGO[i].tag == "BuyShapeButton")
                {
                    buyButton = tempChildrenGO[i].gameObject;
                }

                if (tempChildrenGO[i].tag == "SetShapeButton")
                {
                    setButton = tempChildrenGO[i].gameObject;
                }

            }

            //disable the price && buy button
            thePriceGO.SetActive(false);
            buyButton.SetActive(false);
            AddSetButtonToPanel(bt.transform.parent.gameObject);
            //setButton.SetActive(true);

            ////enable grid layout element (set width and hight to 350x55)
            //GridLayoutGroup gridLayout;
            //gridLayout = parentGameObject.GetComponent<GridLayoutGroup>();
            //gridLayout.enabled = true;

            SetWhichColorWasBought();
            saveAndLoadScript.Save();

            gmScript.boughtAudio.SetActive(false);
            gmScript.boughtAudio.SetActive(true);
        }

    }

    void SetShapeButtonTapped(Button bt)
    {
           
        GameObject parrentGO = bt.transform.parent.gameObject;
        Sprite newSpriteToUse = null;

        //ShapeButton GameObject find

        int children =parrentGO.transform.childCount;
        for (int i = 0; i < children; i++)
        {
            Transform tempChildrenTransform;
            tempChildrenTransform = parrentGO.transform.GetChild(i);
            if (tempChildrenTransform.tag == "OneShopShapeButton")
            {
                newSpriteToUse = tempChildrenTransform.gameObject.GetComponent<Image>().sprite;
            }

        }

        //seeting normal or bonus button to taped
        int index = DragColorManager.GettingNumberFromString(parrentGO.name);


        //create bonus or normal panel and SAVE
        CreateWhichButtonShapePanel(index , newSpriteToUse , bt);


        gmScript.shapeSetAudio.SetActive(false);
        gmScript.shapeSetAudio.SetActive(true);


    }

    void SetSpriteToBonusPanel(GameObject mainPanel, Sprite newSpriteToUse)
    {
        GameObject mainButton, childrenButton = null;


        //getting Main Button and childrenButton
        mainButton = currentShapeInUse;

        int childrenOfABounsPanel = currentShapeInUse.transform.childCount;
        //get children button
        for (int i = 0; i < childrenOfABounsPanel; i++)
        {
            Transform tempChildrenTransform;
            tempChildrenTransform = currentShapeInUse.transform.GetChild(i);
            if (tempChildrenTransform.name == "ChildrenBonusPanel")
            {
                childrenButton = tempChildrenTransform.transform.gameObject;
            }
        }

        // setting new sprite to bouns panel (current shape in use) main and children
        mainButton.GetComponent<Image>().sprite = newSpriteToUse;
        if (childrenButton != null)
        { childrenButton.GetComponent<Image>().sprite = newSpriteToUse; }

    }


    void SetWhichColorWasBought()
    {

        for (int i = 0; i < boughtShapes.Length; i++)
        {
            if (boughtShapes[i] != null)
            {
                nameOfBoughtShapes[i] = (DragColorManager.GettingNumberFromString(boughtShapes[i].name)).ToString();

            }
        }
    }
     


    // set Buy! button to Set! button

    void DisableBuyButtonAndActiveSetButton()
    {

        //for each bought button
        for (int i = 0; i < boughtShapes.Length; i++)
        {

            if (boughtShapes[i] != null)
            {
                ////set size to 350x55 gridLayoutEnable
                //GridLayoutGroup gridLayout;
                //gridLayout = boughtShapes[i].GetComponent<GridLayoutGroup>();
                //gridLayout.enabled = true;



                //disable buy button and Price text && find set button
                Button[] btArr = new Button[4];
                Text[] textArr = new Text[4];
                //Image[] imageArr = new Image[4];

                btArr = boughtShapes[i].GetComponentsInChildren<Button>();
                textArr = boughtShapes[i].GetComponentsInChildren<Text>();
               // imageArr = boughtShapes[i].GetComponentsInChildren<Image>();


                //check if component is correct


                Button tempBuyButton = null;
                //Button tempSetButton = null;
                Text tempText = null;
               // Image tempImage = null;
                //choosing the right text(Price) and the right button(Buy)
                for (int j = 0; j < btArr.Length; j++)
                {
                    if (btArr[j].transform.tag == "BuyShapeButton")
                    { tempBuyButton = btArr[j]; }                    
                }

                for (int k = 0; k < textArr.Length; k++)
                {
                    //**************************//
                    if (textArr[k].transform.gameObject.tag == "ShapePrice")
                    { tempText = textArr[k]; }
                }


                AddSetButtonToPanel(boughtShapes[i]);
                tempBuyButton.transform.gameObject.SetActive(false);
                tempText.transform.gameObject.SetActive(false);
               // tempSetButton.transform.gameObject.SetActive(true);

            }
        }
    }

    void AddSetButtonToPanel(GameObject go)
    {
        GameObject tempSetButton;
        tempSetButton = Instantiate( Resources.Load("UI/ShopUI/SetShapeButton") )as GameObject;
        tempSetButton.transform.parent = go.transform;
        tempSetButton.transform.localScale = new Vector3(1f, 1f, 1f);
        tempSetButton.transform.localPosition = new Vector3(132.5f, 0f, 0f);        
        AddListener(tempSetButton.GetComponent<Button>(), tempSetButton.transform.tag);
    }


   public void CheckWhichShapeIsInUseAndSet(bool shapeShopOpen)
    {
        // the shape which will be set as a bonus button
        GameObject shapeToSet;
        //string tempString;
        int numberOfUsingShape = 0;
        //int children = currentShapeInUse.transform.childCount;
        //for (int i = 0; i < children; i++)
        //{
        //    Transform tempChildrenTransform;
        //    tempChildrenTransform = currentShapeInUse.transform.GetChild(i);
        //    if (tempChildrenTransform.name == "NumberInUse")
        //    {
        //        numberInUse = tempChildrenTransform.gameObject.GetComponent<Text>().text;
        //        tempString = numberInUse;
        //        numberOfUsingShape = int.Parse(tempString); 
        //    }
        //}


        int.TryParse(gmScript.GlobalNumberInUse ,out numberOfUsingShape) ;
        if (gmScript.GlobalNumberInUse == null)
            numberOfUsingShape = 1;
        
            shapeToSet = go_Shapes[numberOfUsingShape];
            if(shopShapeButtonStart)
            {
                int children = boughtShapes[numberOfUsingShape].transform.childCount;
                for (int i = 0; i < children; i++)
                {
                    Transform tempChildrenTransform;
                    tempChildrenTransform = boughtShapes[numberOfUsingShape].transform.GetChild(i);
                    if (tempChildrenTransform.tag == "SetShapeButton")
                    {
                        go_currentShapeSet = tempChildrenTransform.transform.parent.gameObject; 

                        //Color tempColor = tempChildrenTransform.GetComponent<Image>().color;
                        //tempColor = colorOfNowUsingShape;

                        tempChildrenTransform.GetComponent<Button>().image.color = colorOfNowUsingShape;

                        currentShapeSetButton = tempChildrenTransform.GetComponent<Button>();

                        currentShapeInUseTextComponent =  tempChildrenTransform.GetChild(0).GetComponent<Text>();
                        currentShapeInUseTextComponent.text = st_textOfShapeInUse;
                    }
                }
            }

        

        SetSpriteToBonusPanel(currentShapeInUse, shapeToSet.GetComponent<Image>().sprite);    
            
    }





    //change shape of basic buttons
    void CreateWhichButtonShapePanel(int newIndex , Sprite newSpriteToUse , Button bt)
    {
        GameObject panelSpawned;
        Transform panelTransform;
        Button normalButton, bonusButton;
        RectTransform rectTransform;

        panelSpawned = Instantiate(Resources.Load("UI/ShopUI/Changing Shape Panel")) as GameObject;
        panelTransform = panelSpawned.transform;
        rectTransform = panelSpawned.GetComponent<RectTransform>();
        //parent to hud
        panelTransform.parent = GameObject.FindGameObjectWithTag("HUD").transform;
        panelTransform.localScale = new Vector3(1f, 1f, 1f);
        panelTransform.localPosition = new Vector3(0f, 0f, 0f);
        rectTransform.offsetMin = new Vector2(0, 0);
        rectTransform.offsetMax = new Vector2(0, 0);


        //getting normal and bonus button

        normalButton = panelSpawned.transform.GetChild(0).transform.GetChild(0).gameObject.GetComponent<Button>();
        normalButton.onClick.AddListener(() => TapChangeShapeButton(normalButton.transform.gameObject , newIndex , panelSpawned , newSpriteToUse , bt));

        bonusButton = panelSpawned.transform.GetChild(0).transform.GetChild(1).gameObject.GetComponent<Button>();
        bonusButton.onClick.AddListener(() => TapChangeShapeButton(bonusButton.transform.gameObject , newIndex , panelSpawned , newSpriteToUse , bt));
    }

    void TapChangeShapeButton(GameObject bt_go,int newIndex , GameObject panel , Sprite newSpriteToUse , Button bt )
    {
        

        if (bt_go.name == "NormalButton")
        {
            
           // Debug.Log("normal taped");
            ChangeBasicButtonSprite(newIndex);
            shopManagerScript.UpdateUsingBasicSprites(newSpriteToUse);
            Destroy(panel);
           

        }
        else
        {
            GameObject parrentGO = bt.transform.parent.gameObject;
          ////  Debug.Log("boonus taped");
            Destroy(panel);
          


            // set "Set button" taped text to already "Set!" <- now in use
            if (go_currentShapeSet != parrentGO)
            {

                Text tempSetText;
                Color prevUsingColor;

                tempSetText = bt.transform.GetChild(0).GetComponent<Text>();
                tempSetText.text = st_textOfShapeInUse;

                prevUsingColor = bt.image.color;
                // set color to red
                bt.image.color = colorOfNowUsingShape;
                //and back change prev color to normal
                currentShapeSetButton.image.color = prevUsingColor;

                currentShapeInUseTextComponent.text = st_StandardTextOfSetting;


                //set just now changed GO.text comp to variable holding that value
                currentShapeInUseTextComponent = tempSetText;

                go_currentShapeSet = parrentGO;

                currentShapeSetButton = bt;

                //set the number of set sprite to numberInUse
                /*numberInUse*/
                gmScript.GlobalNumberInUse = DragColorManager.GettingNumberFromString(parrentGO.name).ToString();
                //= numberInUse;

                SetSpriteToBonusPanel(currentShapeInUse, newSpriteToUse);
            }                       
        }

        saveAndLoadScript.Save();
    }


    //set basic button sprite

    void ChangeBasicButtonSprite(int index)
    {

        gmScript.int_UsingSpriteIndex = index;
    }


   public Sprite SetNewBasicSprite(int index)
    {
        if (index < 0 && index > sprite_Shapes.Length)
        { index = 0; }
        Sprite temp;
        temp = go_Shapes[index].GetComponent<Image>().sprite;
        return temp;
    }
    
}
