﻿using UnityEngine;
using System.Collections;

public class DragColorManager : MonoBehaviour {


    public GameObject[] colorSlots;// = new GameObject[9];
   // public GameObject parentPanel;
    //public GameObject dragginGameObject;
    //Transform[] childrenTransforms;
    int childrenCount;



    // Use this for initialization
    void Start ()
    {
        childrenCount = transform.childCount;
        colorSlots = new GameObject[childrenCount];
        //childrenTransforms = new Transform[transform.childCount];

        FindChildren();
	}
	

    void FindChildren()
    {
        int children = transform.childCount;
        for (int i = 0; i < children; ++i)
            colorSlots[i] = transform.GetChild(i).gameObject;
    }


    public void SettingOrderInPanel(GameObject dragginGameObject, Transform parentPanel)
    {

        string st_number;
        int numberInOrder;

        st_number = dragginGameObject.name;
        numberInOrder = GettingNumberFromString(st_number);
        dragginGameObject.transform.SetParent(parentPanel);
        dragginGameObject.transform.SetSiblingIndex(numberInOrder);
    }


   public static int GettingNumberFromString(string st_num )
    {
        int nmb;
        string tempst ="" ;
        int counter = 0;
        do
        {
            tempst += st_num[counter];
            counter++;
        }
        while (st_num[counter] != '_');
        nmb = int.Parse(tempst);
        return nmb;
    }
}
