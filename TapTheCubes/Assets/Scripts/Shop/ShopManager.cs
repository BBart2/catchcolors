﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShopManager : MonoBehaviour
{

    //scripts
    SaveAndLoadColors saveAndLoadScript;
    public ShopShapeButtonManager shopShapeBtManag;
    [HideInInspector]
    public GameObject shopButton;
    [HideInInspector]
    public GameObject shopPanel;
    [HideInInspector]
    public Text moneyText;
    [HideInInspector]
    public Button backButton;
    [HideInInspector]
    public Button colorShopButton;
    [HideInInspector]
    public Button shapeShopButton;

    //Buy specyfic color button
    public GameObject[] buyColorButton = new GameObject[32];
    
    //array of purchased buttons
    public GameObject[] boughtColors = new GameObject[32];      // np 0_Slot,12_Slot... !
    //name of buttons bought
    public string[] nameOfBoughtColors = new string[32];

      

    //amount of money to buy one color 
    int priceForOneColor = 5000;   // 5000

    //stuffs for color panel
    [HideInInspector]
    public GameObject colorPanel;

    //stuffs for Shape panel
    [HideInInspector]
    public GameObject shapePanel;
    


    //variables for showing current button colors PACK
    [HideInInspector]
    public GameObject parrentPanel;
    [HideInInspector]
    public GameObject imageToSpawn;

    //drag&DROP
    [HideInInspector]
    public   GameObject panelToSpawn;  // for the new drag&drop system
    [HideInInspector]
    public   GameObject parrentForDragAndDrop;
    [HideInInspector]
    public   GameObject bonusPanel;


    //upper slot gO's
     public GameObject[] currentColorsInUseArr = new GameObject[6];


    //current basic bt sprite
    Sprite currentBasicBtUsingSprite;






    //VALUE OF CURRENT COLORS
    int currentButtonColor;

    //script gm
    [HideInInspector]
    public GameManager GmScript;

    // COLORS
    Color[] basicColors = new Color[6] { Color.green, Color.blue, Color.yellow, Color.cyan, Color.grey, Color.white };

    //public Color[] firstItemColors = new Color[6] { Color.white, Color.grey, Color.cyan, Color.yellow, Color.blue, Color.green };

   Color[] firstItemColor = new Color[6] { Color.white, Color.red, Color.black, Color.yellow, Color.blue, Color.green };
    //local variable colo
    Color[] currentColor;

   public  int moneyValue;

    //ItemsValuse
    //  int firstItemValue = 10;

    //which item is buy bools
    // public bool b_firstItemBought = false/*, b_secondItemBought = false*/;


    // Use this for initialization
    void Start()
    {

        saveAndLoadScript = GameObject.Find("__Save&LoadManager").GetComponent<SaveAndLoadColors>();


        shopShapeBtManag.StartInitialization();
        

        //listeners!

        shopButton.GetComponent<Button>().onClick.AddListener(() => ShopButtonTapped(shopButton.GetComponent<Button>()));
        backButton.onClick.AddListener(() => BackButtonTapped(backButton));

        colorShopButton.onClick.AddListener(() => ColorShopTapped(colorShopButton));
        shapeShopButton.onClick.AddListener(() => ShapeShopTapped(shapeShopButton));



        //itemsButtons ONCLICK
        //PlayerPrefs.SetInt("firstItemBought", 0);

        //  go_defaultItemButton.GetComponent<Button>().onClick.AddListener(() => DefaultItemButtonTapped(go_defaultItemButton.GetComponent<Button>()));





        


        //check which buttons is bought
        // CheckWhichButtonIsBought();


        //SetUpStartButtonColor();

        //NewCurrentButtonColorsInitialization();
        //set buy/set buttons
        //SetBuyOrSetButton();

    }
    //                         Custom Functions                          //
    //******************************************************************//

        //sorting the array
    void SortingBuyButtonArray(GameObject[] bt)
    {
        GameObject[] tempArray = new GameObject [bt.Length];
        string str_numberOfButton;
        int number;

        for (int i = 0; i < bt.Length; i++)
        {
            str_numberOfButton = bt[i].transform.parent.name;
            number = DragColorManager.GettingNumberFromString(str_numberOfButton);
            tempArray[number] = bt[i];
        }

        for (int i = 0; i < bt.Length; i++)
        {
            bt[i] = tempArray[i];
        }

    }

    //hide ability to buy already bought elements(colors)

    void DisableBuyButtonToAlreadyBoughtButtons()
    {
               
        //for each bought button
        for (int i = 0; i < boughtColors.Length; i++)
        {

            if (boughtColors[i] != null)
            {
                //set size to 350x55 gridLayoutEnable
                GridLayoutGroup gridLayout;
                gridLayout = boughtColors[i].GetComponent<GridLayoutGroup>();
                gridLayout.enabled = true;



                //disable buy button and Price text && panel to make able to drag image
                Button[] btArr = new Button[3];
                Text[] textArr = new Text[3];
                Image[] imageArr = new Image[4];

                btArr = boughtColors[i].GetComponentsInChildren<Button>();
                textArr = boughtColors[i].GetComponentsInChildren<Text>();
                imageArr = boughtColors[i].GetComponentsInChildren<Image>();


                //check if component is correct


                Button tempButton = null;
                Text tempText = null;
                Image tempImage = null;
                //choosing the right text(Price) and the right button(Buy)
                for (int j = 0; j < btArr.Length; j++)
                {
                    if (btArr[j].transform.tag == "BuyColorButton")
                    { tempButton = btArr[j]; }
                }

                for (int k = 0; k < textArr.Length; k++)
                {
                    //**************************//
                    if (textArr[k].transform.gameObject.tag == "Price")
                    { tempText = textArr[k]; }
                }

                //search for blocking dragging color panel and disabel it
                for (int l = 0; l < imageArr.Length; l++)
                {
                    if (imageArr[l].transform.gameObject.tag == "BlockColorPanel")
                    { tempImage = imageArr[l]; }
                }

                tempButton.transform.gameObject.SetActive(false);
                tempText.transform.gameObject.SetActive(false);
                tempImage.transform.gameObject.SetActive(false);

            }
        }
    }

    //adding listeners
    void AddOnClickToBuyColorButton(GameObject[] bt)
    {
        for (int i = 0; i < bt.Length ; i++)
        {
            AddListener(bt[i].GetComponent<Button>());
        }
    }

    void AddListener(Button b)
    {
        b.onClick.AddListener(() => BuyColorButtonTapped(b));
    }


    // Listener to buy color button 
    //and setting bought color to an bought array
    void BuyColorButtonTapped(Button bt)
    {
       // Debug.Log("Clicked " + bt.transform.parent.name);
        // za mało hajsow na zakup
        if(moneyValue < priceForOneColor)
        {
          //  Debug.Log("Go and earn more money!");
            GmScript.badClickAudio.SetActive(false);
            GmScript.badClickAudio.SetActive(true);
        }
        else
        {
     
            //buying button and adding to bought array button
            int number = 0;
            GameObject parentGameObject;
            parentGameObject = bt.transform.parent.gameObject;             
            number = DragColorManager.GettingNumberFromString(parentGameObject.name);
            boughtColors[number] = parentGameObject;
            GmScript.moneyValue -= priceForOneColor;            
            //setting moneyValue to prefab
            //PlayerPrefs.SetInt("moneyPref", moneyValue);
            moneyText.text = GmScript.moneyValue.ToString();

            //get all text component from parent gameobject
            Transform[] tempChildrenGO = new Transform[3];

            tempChildrenGO = parentGameObject.GetComponentsInChildren<Transform>(true);

            //search the specyfic text gameobject(PRICE text) 
            GameObject thePriceGO = null;
            GameObject buyButton = null;
            GameObject blockImagePanel = null;

            for (int i = 0; i < tempChildrenGO.Length; i++)
            {
                if(tempChildrenGO[i].tag == "Price")
                {
                    thePriceGO = tempChildrenGO[i].gameObject;
                }
                if(tempChildrenGO[i].tag == "BuyColorButton")
                {
                    buyButton = tempChildrenGO[i].gameObject;
                }

                if(tempChildrenGO[i].tag == "BlockColorPanel")
                {
                    blockImagePanel = tempChildrenGO[i].gameObject;
                }
                
            }
            
            //disable the price && buy button
            thePriceGO.SetActive(false);
            buyButton.SetActive(false);
            blockImagePanel.SetActive(false);

            //enable grid layout element (set width and hight to 350x55)
            GridLayoutGroup gridLayout;
            gridLayout = parentGameObject.GetComponent<GridLayoutGroup>();
            gridLayout.enabled = true;

            SetWhichColorWasBought();
            SaveColorBought();

            GmScript.boughtAudio.SetActive(false);
            GmScript.boughtAudio.SetActive(true);

            //check if we need tutorial
            int tutValue = PlayerPrefs.GetInt("ShopTutorialPassed");
            if(tutValue != 1)
            {
                GameObject tempTutPanel;
                tempTutPanel = (GameObject)Instantiate(Resources.Load("Tutorial/ShopTut"));

                tempTutPanel.transform.parent = GameObject.FindGameObjectWithTag("HUD").transform;
                tempTutPanel.transform.localScale = new Vector3(1, 1, 1);
                tempTutPanel.transform.localPosition = new Vector3(500, 300 , 0);

                PlayerPrefs.SetInt("ShopTutorialPassed", 1);

            }

        }
        
    }

    //save bought button array to file
    void SaveColorBought()
    {
        saveAndLoadScript.Save();
    }

    

    //function to check which button was bought and set it to beeing able to drag

    void SetWhichColorWasBought()
    {      
                
        for (int i = 0; i < boughtColors.Length; i++)
        {
            if(boughtColors[i] != null)
            {
                nameOfBoughtColors[i] = (DragColorManager.GettingNumberFromString(boughtColors[i].name)).ToString();
                
            }
        }
    }

    //*****************************  initializations  ***************************************************/

    public void NewCurrentButtonColorsInitialization()
    {
        int index = 0;
        GameObject currentPanel;

        for (int j = 0; j < 6; j++)
        {
            Color currentButtonColor;
            currentButtonColor = GmScript.buttonsColorsArray[j];

            CurrentUsingBasicSprite();


            currentPanel = (GameObject)Instantiate(panelToSpawn);
            currentPanel.GetComponent<Image>().sprite = currentBasicBtUsingSprite;

            currentPanel.transform.SetParent(parrentForDragAndDrop.transform);
            //currentPanel.transform.parent = parrentForDragAndDrop.transform;
            RectTransform rectTr = currentPanel.GetComponent<RectTransform>();
            rectTr.anchoredPosition = new Vector3(8f + (j * 70), 0f, 0f);
            currentPanel.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);

            currentPanel.GetComponent<Image>().color = currentButtonColor;
            currentPanel.name = "Slot " + j;
            //currentImage.tag = "EmptyButton";
            currentColorsInUseArr[index] = currentPanel;
            index++;

        }


        //set bonus button color
        bonusPanel.GetComponent<Image>().color = GmScript.bonusColor;


    }

    //void InitializeCurrentButtonColors()
    //{
    //    int index = 0;
    //    GameObject currentImage;
    //    //float startXpos = startImagepos.transform.position.x + 8;
    //    //float startYpos = startImagepos.transform.position.y;
    //    //imagePos = new Vector2(startXpos, startYpos);

    //    for (int j = 0; j < 6; j++)
    //    {
    //        Color currentButtonColor;
    //        currentButtonColor = GmScript.buttonsColorsArray[j];
    //        //imagePos = new Vector2(imagePos.x + 35, imagePos.y);
    //        // currentImage = (GameObject)Instantiate(image, imagePos, Quaternion.identity);
    //        currentImage = (GameObject)Instantiate(imageToSpawn);
    //        currentImage.transform.parent = parrentPanel.transform;
    //        RectTransform rectTr = currentImage.GetComponent<RectTransform>();
    //        rectTr.anchoredPosition = new Vector3(8f + (j * 70), 0f, 0f);
    //        currentImage.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);


    //        currentImage.GetComponent<Image>().color = currentButtonColor;
    //        //currentImage.tag = "EmptyButton";
    //        currentColorsInUseArr[index] = currentImage;
    //        index++;
    //    }

    //}

    //void CheckWhichButtonIsBought()
    //   {
    //       int tempCheckValue;

    //       tempCheckValue = PlayerPrefs.GetInt("firstItemBought");  
    //       if(tempCheckValue == 1)                                   // check if 1st item were bought 
    //       {
    //           b_firstItemBought = true;
    //          // Debug.Log("set button should be visable");
    //       }
    //       else
    //       {
    //           b_firstItemBought = false;
    //           Debug.Log("buy button should be visable");
    //       }
    //   }


    //   void SetBuyOrSetButton()
    //   {
    //       if(b_firstItemBought)
    //       {
    //          go_firstItemBuyButton.SetActive(false);   //off buy on set && onclick

    //           go_firstItemSetButton.SetActive(true);

    //           go_firstItemSetButton.GetComponent<Button>().onClick.AddListener(() => FirstItemSetButtonTapped(go_firstItemSetButton.GetComponent<Button>()));

    //           Debug.Log("Activating set button");

    //       }

    //       else
    //       {
    //           go_firstItemBuyButton.SetActive(true);   //off buy on set && onclick
    //           go_firstItemBuyButton.GetComponent<Button>().onClick.AddListener(() => FirstItemBuyButtonTapped(go_firstItemBuyButton.GetComponent<Button>()));

    //           go_firstItemSetButton.SetActive(false);

    //           Debug.Log("acitating BUY Button");

    //       }
    //   }

    public void SetUpStartButtonColor()
    {
       // currentButtonColor = PlayerPrefs.GetInt("currentButtonColorPref");

       // currentColor = SetButtonColors(currentButtonColor);

        foreach (Color c in GmScript.buttonsColorsArray)
        {
            GmScript.buttonsColorsArray = currentColor;
        }
    }


    //  LISTENERS!!!

    void ShopButtonTapped(Button bt)
    {
        shopPanel.SetActive(true);
        //SetBuyOrSetButton();
       // InitializeCurrentButtonColors();


        //find every buy color button
        buyColorButton = GameObject.FindGameObjectsWithTag("BuyColorButton");
        SortingBuyButtonArray(buyColorButton);
        // Add onclick event to buy color button
        AddOnClickToBuyColorButton(buyColorButton);


        //loading which color was bought
        saveAndLoadScript.LoadBoughtColorArray(nameOfBoughtColors, boughtColors, buyColorButton);

        DisableBuyButtonToAlreadyBoughtButtons();
    }

    void BackButtonTapped(Button bt)
    {
        GmScript.HideAd();
        GmScript.DestroyBanner();
        saveAndLoadScript.Save();
       // Debug.Log("BackTapped");
        Application.LoadLevel(Application.loadedLevel);
    }

    void ColorShopTapped(Button bt)
    {
        colorPanel.SetActive(true);
        shapePanel.SetActive(false);
        
    }

    void ShapeShopTapped(Button bt)
    {        
        shapePanel.SetActive(true);
        //saveAndLoadScript.LoadBoughtShapeArray(shopShapeBtManag.nameOfBoughtShapes, shopShapeBtManag.boughtShapes, shopShapeBtManag.shapePanels);

        colorPanel.SetActive(false);
        
        
    }

    public void SetMoney()
    {

        moneyValue = GmScript.moneyValue;

        moneyText.text =  moneyValue.ToString();
    }
    //// initialize current button color
    //Color[] SetButtonColors(int number)   // zmienic! 
    //{
    //    Color[] tempArrColor = null;
    //    switch (number)
    //    {
    //        case 1:
    //            foreach (Color c in firstItemColor)
    //            { tempArrColor = firstItemColor; }

    //            break;

    //        default:
    //            foreach (Color c in basicColors)
    //            { tempArrColor = basicColors; }
    //            break;

    //    }

    //    return tempArrColor;
    //}

    //void DefaultItemButtonTapped(Button bt)
    //{
    //    foreach (Color c in GmScript.buttonsColorsArray)
    //    {
    //        GmScript.buttonsColorsArray = basicColors;
    //    }

    //    currentButtonColor = 0;

    //    PlayerPrefs.SetInt("currentButtonColorPref", currentButtonColor);

    //    InitializeCurrentButtonColors();

    //}




    //buttons Tapped

    //void FirstItemBuyButtonTapped(Button bt)
    //{
    //    if(moneyValue < firstItemValue)
    //    {
    //        Debug.Log("Not able to buy first item!");

    //    }
    //    else
    //    {
    //        Debug.Log("Item 1st bought!");

    //        moneyValue -= firstItemValue;

    //        //moneyValue += 5000;

    //        PlayerPrefs.SetInt("moneyPref", moneyValue);

    //        moneyText.text = "Money : " + PlayerPrefs.GetInt("moneyPref") + " $";

    //        foreach(Color c in GmScript.buttonsColorsArray)
    //        {
    //            GmScript.buttonsColorsArray = firstItemColor;
    //        }

    //        currentButtonColor = 1;

    //        PlayerPrefs.SetInt("currentButtonColorPref", currentButtonColor);

    //        PlayerPrefs.SetInt("firstItemBought", 1);

    //        b_firstItemBought = true;

    //        go_firstItemSetButton.SetActive(true);       // zmiana z buy na set po zakupie !
    //        go_firstItemBuyButton.SetActive(false);

    //        InitializeCurrentButtonColors();

    //    }
    //}

    //void FirstItemSetButtonTapped(Button bt)
    //{

    //  //  Debug.Log("Set first item color!");

    //    foreach (Color c in GmScript.buttonsColorsArray)
    //    {
    //        GmScript.buttonsColorsArray = firstItemColor;
    //    }

    //    currentButtonColor = 1;

    //    PlayerPrefs.SetInt("currentButtonColorPref", currentButtonColor);        

    //    InitializeCurrentButtonColors();
    //}


    void CurrentUsingBasicSprite()
    {
        currentBasicBtUsingSprite = shopShapeBtManag.SetNewBasicSprite(GmScript.int_UsingSpriteIndex);
    }

    public void UpdateUsingBasicSprites(Sprite newSprite)
    {
        for (int i = 0; i < currentColorsInUseArr.Length; i++)
        {
            currentColorsInUseArr[i].GetComponent<Image>().sprite = newSprite;
        }
    }

}
