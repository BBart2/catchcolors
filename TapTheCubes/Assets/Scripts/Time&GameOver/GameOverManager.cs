﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{

    SaveAndLoadColors saveAndLoadScript;
    ScoreManager scoreMgScript;
    GameManager gmScript;
    AdManager adManagScript;
    GiftizMissions giftizMissionScript;

    GameObject panelPrefab;

    float restartTimeDelay = 5f;
    float restartTimer = 0f;
    public GameObject screenFader;
    public GameObject highScoreGO;
    GameObject playAdButton_GO;

    GameObject adPanel, adText, adRewardText, scorePanel_go, earnedMoneyPanel_go, bonusMoneyPanel_go, currentMoneyPanel_go, allMoneyPanel_go, highScoreText_go, gameOverText_go;
    Button playAgainButton, adButton;
    public Text moneyText;

    public GameObject gameButtonPanel, gamePaternPanel, Time_go, gamePauseButton;

    public bool newHighscore = false;
    public Text scoreText;

    bool panelSpawned = false;
    bool scoreSet = false;
    //money value before adding next game money value's 
    int prevMoneyValue;
    public int score;
    bool alreadySpawned = false;

    Image imageFader;

    Animator anim;

    Text highScoreText;

    // Use this for initialization
    void Start()
    {
        adManagScript = GameObject.FindGameObjectWithTag("AdManager").GetComponent<AdManager>();
        gmScript = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        scoreMgScript = GameObject.FindGameObjectWithTag("ScoreManger").GetComponent<ScoreManager>();
        saveAndLoadScript = GameObject.FindGameObjectWithTag("Save&Load").GetComponent<SaveAndLoadColors>();
        giftizMissionScript = GameObject.FindGameObjectWithTag("Giftiz").GetComponent<GiftizMissions>();

        imageFader = screenFader.GetComponent<Image>();
        highScoreText = highScoreGO.GetComponent<Text>();
        prevMoneyValue = gmScript.moneyValue;
    }

    //// Update is called once per frame
    //void Update()
    //{

    //    if (GameTimeMaganer.time <= 0f)
    //    {          
    //        //imageFader.enabled = true;
    //        //anim.SetTrigger("GameOver");



    //        //// .. increment a timer to count up to restarting.
    //        //restartTimer += Time.deltaTime;

    //        highScoreText.text = "High Score : " + PlayerPrefs.GetInt("highScorePref");



    //        // .. if it reaches the restart delay...
    //        if(!panelSpawned )
    //        {
    //            panelSpawned = true;

    //            // .. then reload the currently loaded level.
    //            SpawnGameOverPanel();

    //        }
    //    }
    //}

    public void SpawnGameOverPanel()
    {
        if (!alreadySpawned)
        {
            // gmScript.gameAudio.SetActive(false);
          //  gmScript.ShowAd();
            TurnOffGameUI();
            alreadySpawned = true;
            GameObject mainPanel = Instantiate(Resources.Load("UI/GameOver/UIP_Report2")) as GameObject;
            GameObject childrenPanel = mainPanel.transform.GetChild(1).gameObject;

            GameObject scorePanel = mainPanel.transform.GetChild(0).gameObject;
            GameObject newHighscorePanel = mainPanel.transform.GetChild(4).gameObject;
            //**************************//


            Transform panelTransform = mainPanel.transform;
            RectTransform rectTransform;


            //anim = mainPanel.GetComponent<Animator>();

            panelTransform = mainPanel.transform;
            rectTransform = mainPanel.GetComponent<RectTransform>();
            //parent to hud
            panelTransform.parent = GameObject.FindGameObjectWithTag("HUD").transform;
            panelTransform.localScale = new Vector3(1f, 1f, 1f);
            panelTransform.localPosition = new Vector3(0f, 0f, 0f);
            //rectTransform.offsetMin = new Vector2(0, 0);
            // rectTransform.offsetMax = new Vector2(0, 0);

            //find all GO's
            //ads

            //newUIPanel
            playAdButton_GO = mainPanel.transform.GetChild(6).gameObject;



            //adPanel = mainPanel.transform.GetChild(0).gameObject;
            //playAdButton_GO = childrenPanel.transform.GetChild(3).gameObject;
            adButton = playAdButton_GO.GetComponent<Button>();
            // adText = playAdButton_GO.transform.GetChild(0).gameObject;
            adRewardText = playAdButton_GO.transform.GetChild(0).gameObject;

            //children panel
            playAgainButton = mainPanel.transform.GetChild(5).GetComponent<Button>();

            // scorePanel_go = childrenPanel.transform.GetChild(1).gameObject;
            // allMoneyPanel_go = childrenPanel.transform.GetChild(2).gameObject;

            //score number : 150
            scoreText = scorePanel.transform.GetChild(0).GetComponent<Text>();

            highScoreText = scorePanel.transform.GetChild(1).GetComponent<Text>();

            //all money value
            //allMoneyPanel_go = childrenPanel.transform.GetChild(2).gameObject;

            //gameOverText_go = mainPanel.transform.GetChild(1).gameObject;

            //highScoreText_go = childrenPanel.transform.GetChild(1).transform.GetChild(3).gameObject;

            ////children panels -- finding go;s HardCode <3
            //scorePanel_go = childrenPanel.transform.GetChild(0).gameObject;
            //earnedMoneyPanel_go = childrenPanel.transform.GetChild(1).transform.GetChild(0).gameObject;
            //bonusMoneyPanel_go = childrenPanel.transform.GetChild(1).transform.GetChild(1).gameObject;
            //currentMoneyPanel_go = childrenPanel.transform.GetChild(1).transform.GetChild(2).gameObject;

            //allMoneyPanel_go = childrenPanel.transform.GetChild(3).transform.GetChild(0).gameObject;

            //playAgainButton = childrenPanel.transform.GetChild(4).GetComponent<Button>();
            //adButton = childrenPanel.transform.GetChild(5).transform.GetChild(1).GetComponent<Button>();
            //playAdButton = childrenPanel.transform.GetChild(5).transform.GetChild(1).gameObject;



            playAgainButton.onClick.AddListener(() => PlayAgainTaped());
            adButton.onClick.AddListener(() => PlayAd());
            //adPanel = childrenPanel.transform.GetChild(5).gameObject;


            // if (!(gmScript.CheckIfWeShouldSpawnAd()))   // unnity
              if ((AdBuddizBinding.RewardedVideo.IsReadyToShow() == false) || (gmScript.CheckIfWeShouldSpawnAd() == false))
              {                
                  playAdButton_GO.SetActive(false);
              }
              
            if (newHighscore)
            {
                newHighscorePanel.SetActive(true);
            }

            //setting values to panels etc

            highScoreText.text = "Best score: " + scoreMgScript.highScore.ToString();


            // setting score to just earned score in ended game
            scoreText.text = score.ToString();



            // float multiply = 10f;            
            int thatGameMoney = System.Convert.ToInt32(score);

            // earnedMoneyPanel_go.transform.GetChild(1).GetComponent<Text>().text = thatGameMoney.ToString() + "$";


            //kasa za tapniecie bonus buttona += 10$

            //  bonusMoneyPanel_go.transform.GetChild(1).GetComponent<Text>().text = gmScript.bonusMoneyValue.ToString() + "$";


            //****************************//
            // currentMoneyPanel_go.transform.GetChild(1).GetComponent<Text>().text = gmScript.moneyValueBeforeNewGame.ToString() + "$";


            //*****************************************************************************//
            // int moneyWithBonuses = thatGameMoney + gmScript.bonusMoneyValue + gmScript.moneyValueBeforeNewGame;

            int bonusMultiplay = 1;
            gmScript.moneyValue += thatGameMoney * bonusMultiplay;

            // moneyText = allMoneyPanel_go.transform.GetChild(0).transform.GetChild(1).GetComponent<Text>();

            // moneyText.text = gmScript.moneyValue.ToString() + "$";

            saveAndLoadScript.Save();

            gmScript.gameOverAudio.SetActive(true);

            //  anim.SetTrigger("GameOver");

            //check gifiz missions
            giftizMissionScript.CheckMissions(score);

        }

    }

    void PlayAd()
    {
        //adManagScript.ShowAd("rewardedVideo");    // unity AD!
        adManagScript.ShowBuddizRewardAd(); // adbuzz ad!
    }

    void PlayAgainTaped()
    {
        //google ads
        gmScript.HideAd();
        gmScript.DestroyBanner();
        Application.LoadLevel(Application.loadedLevel);
    }
    public void TurnOffAdButton()
    {
        playAdButton_GO.transform.GetChild(0).gameObject.SetActive(true);
        playAdButton_GO.transform.GetChild(2).gameObject.SetActive(false);
        playAdButton_GO.transform.GetChild(3).gameObject.SetActive(false);
    }


    void TurnOffGameUI()
    {
        gameButtonPanel.SetActive(false);
        gamePaternPanel.SetActive(false);

        //turn off time UI and Score UI
        GameObject score = GameObject.FindGameObjectWithTag("ScoreManger");
        Color tempcolor = Color.white;
        tempcolor.a = 0;
        score.GetComponent<Text>().color = tempcolor;
        Time_go.SetActive(false);

        gamePauseButton.SetActive(false);
    }

}
