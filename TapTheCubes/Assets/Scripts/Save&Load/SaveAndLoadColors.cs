﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveAndLoadColors : MonoBehaviour
{

    public GameManager GameManagScript;
    public ShopManager shopManagerScript;
    public ShopShapeButtonManager shopShapeButtonManagScript;
    public static SaveAndLoadColors control;


    // path
    string fileName = "/gameData.dat";
    



    //public Color[] colorsArr = new Color[2] { Color.red, Color.black };


    // Use this for initialization
    void Start()
    {
        GameManagScript = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        shopManagerScript = GameObject.FindGameObjectWithTag("HUD").GetComponent<ShopManager>();
        


        if (control == null)
        {
            DontDestroyOnLoad(gameObject);
            control = this;
        }
        else if (control != this)
        {
            Destroy(gameObject);
        }

    }

    //void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.S))
    //    {
    //        Debug.Log("Saving!");
    //        Save();
    //    }
    //    else if (Input.GetKeyDown(KeyCode.L))
    //    {
    //        Debug.Log("Loading!");
    //        Load();
    //    }
    //}



    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + fileName);

        PlayerData data = new PlayerData();

        //bonus COLOR
        data.bonusColor[0] = GameManagScript.bonusColor.r;
         data.bonusColor[1] = GameManagScript.bonusColor.g;
         data.bonusColor[2] = GameManagScript.bonusColor.b;
         data.bonusColor[3] = GameManagScript.bonusColor.a;



        //save last ad
        data.lastWatchedAd = GameManagScript.lastAdWatchedTime ;


        // current using sprite for normal bts
        data.int_indexOfNormBtSprite  = GameManagScript.int_UsingSpriteIndex;

        //save money $$$$$$$$$$$$$$$$
        data.money = GameManagScript.moneyValue;

        //save shape
        string tempString = GameManagScript.GlobalNumberInUse;
        int tempInt;
        int.TryParse(tempString, out tempInt);
        data.shapeInUseNumber = tempInt;

        //save colors
         data.SaveNumberOfBoughtColors(shopManagerScript.nameOfBoughtColors);

         //save shapes
         data.SaveNumberOfBoughtShapes(shopShapeButtonManagScript.nameOfBoughtShapes);

        //rest colors

        for (int i = 0; i < GameManagScript.buttonsColorsArray.Length; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                switch (j)
                {
                    case 0:
                       data.colorsForAllArr[i][j] = GameManagScript.buttonsColorsArray[i].r;
                        break;

                    case 1:
                        data.colorsForAllArr[i][j] = GameManagScript.buttonsColorsArray[i].g;
                        break;

                    case 2:
                        data.colorsForAllArr[i][j] = GameManagScript.buttonsColorsArray[i].b;
                        break;

                    case 3:
                        data.colorsForAllArr[i][j] = GameManagScript.buttonsColorsArray[i].a;
                        break;

                        
                }

                
            }
            
        }
        int saveGameCounter = PlayerPrefs.GetInt("SaveGameCounter");
        saveGameCounter++;
        PlayerPrefs.SetInt("SaveGameCounter", saveGameCounter);
        Debug.Log("saving!");
        bf.Serialize(file, data);
        file.Close();
    }

    public void Load()
    {
        int saveGameCounter = PlayerPrefs.GetInt("SaveGameCounter");
        if (File.Exists(Application.persistentDataPath + fileName) && saveGameCounter > 0 )
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + fileName, FileMode.Open);
            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();


            //last ad watched
            GameManagScript.lastAdWatchedTime = data.lastWatchedAd;

            // current using sprite for normal bts

            if (data.int_indexOfNormBtSprite >= 0 || data.int_indexOfNormBtSprite > 21  )
            {
                GameManagScript.int_UsingSpriteIndex = data.int_indexOfNormBtSprite;
            }
            else
            { GameManagScript.int_UsingSpriteIndex = 0; }

            // money $$$$ 
            if (data.money > 0)
            {
                GameManagScript.moneyValue = data.money;
            }
            else
                GameManagScript.moneyValue = 0;
            //Bonus Color 

            GameManagScript.bonusColor.r = data.bonusColor[0];
            GameManagScript.bonusColor.g = data.bonusColor[1];
            GameManagScript.bonusColor.b = data.bonusColor[2];
            GameManagScript.bonusColor.a = data.bonusColor[3];

            //load shape
            if (data.shapeInUseNumber.ToString().Length > 0)
            { GameManagScript.GlobalNumberInUse = data.shapeInUseNumber.ToString(); }
            else GameManagScript.GlobalNumberInUse = "1";
            

            //  data.FillArrayWithNulls();
            //bought colors
            if(shopManagerScript.nameOfBoughtColors.Length > 0 && shopManagerScript != null)
            data.LoadNumbersOfBoughtColors(shopManagerScript.nameOfBoughtColors);
            //bought shapes
            data.LoadNumberOfBoughtShapes(shopShapeButtonManagScript.nameOfBoughtShapes);


            //rest Colors
            for (int i = 0; i < GameManagScript.buttonsColorsArray.Length; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    switch (j)
                    {
                        case 0:
                            GameManagScript.buttonsColorsArray[i].r = data.colorsForAllArr[i][j];
                            break;

                        case 1:
                            GameManagScript.buttonsColorsArray[i].g = data.colorsForAllArr[i][j];
                            break;

                        case 2:
                            GameManagScript.buttonsColorsArray[i].b =  data.colorsForAllArr[i][j];
                            break;

                        case 3:
                            GameManagScript.buttonsColorsArray[i].a = data.colorsForAllArr[i][j];
                            break;


                    }


                }

            }

           // Debug.Log("Loading!");
        }
    }

    //load bought GO array 
   public void LoadBoughtColorArray(string[] numbers , GameObject[] boughtColorGOArray , GameObject[] allBuyButtons )
    {
        if (numbers.Length > 0)
        {
            for (int i = 0; i < allBuyButtons.Length ; i++)
            {
                if (numbers[i] != "")
                {
                    //int number;
                    //number = int.Parse(numbers[i]);

                    boughtColorGOArray[i] = allBuyButtons[i].transform.parent.gameObject;

                }

            }
        }
    }

    //load bought Shape array
    public void LoadBoughtShapeArray(string[] numbers, GameObject[] boughtShapeGOArray, GameObject[] allShapePanel)
    {

        if (numbers.Length > 0)
        {
            boughtShapeGOArray[0] = allShapePanel[0];
            boughtShapeGOArray[1] = allShapePanel[1];
            for (int i = 2; i < allShapePanel.Length ; i++)
            {
                if (numbers[i] != "")
                {
                    // int number;
                    //number = int.Parse(numbers[i]);

                    boughtShapeGOArray[i] = allShapePanel[i];

                }

            }
        }
    }
}



[System.Serializable]
class PlayerData : System.Object
{
    /////public Color[] colors = new Color[2];

    public float[][] colorsForAllArr = new float[6][];
    public float[] bonusColor = new float[4];
    //variables for saving bought color    
    string[] st_tempColorsBought = new string[32];
    string[] st_tempShapesBought = new string[22];

    //current shape using for normal buttons
    public int int_indexOfNormBtSprite;

    public DateTime lastWatchedAd;

    public bool didEverPlay;

    // money!! $$$
    public int money;

    //current shape in use
    public int shapeInUseNumber;

    public PlayerData()
    {
        for (int i = 0; i < 6; i++)
        {
            colorsForAllArr[i] = new float[4];
        }
    }

    public float[] firstColor = new float[4];
    public float[] secondColor = new float[4];

    //saving bought colors from shop
    public void SaveNumberOfBoughtColors(string[] numberOfColorsBought)
    {
        for (int i = 0; i < numberOfColorsBought.Length; i++)
        {
            st_tempColorsBought[i] = numberOfColorsBought[i];
        }
    }

    

    public void LoadNumbersOfBoughtColors(string[] numberOfColorsBought)
    {
        
        for (int i = 0; i < numberOfColorsBought.Length; i++)
        {
             numberOfColorsBought[i] = st_tempColorsBought[i] ;
        }
    }


    //saving & loading shapes


    public void SaveNumberOfBoughtShapes(string[] numberOfShapesBought)
    {
        
        for (int i = 0; i < numberOfShapesBought.Length; i++)
        {
            st_tempShapesBought[i] = numberOfShapesBought[i];
        }
    }

    public void LoadNumberOfBoughtShapes(string[] numberOfShapesBought)
    {
        st_tempShapesBought[0] = "0";
        st_tempShapesBought[1] = "1";
        for (int i = 0; i < numberOfShapesBought.Length; i++)
        {
             numberOfShapesBought[i] = st_tempShapesBought[i] ;
        }
    }
}
