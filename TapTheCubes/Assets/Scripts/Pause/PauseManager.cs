﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class PauseManager : MonoBehaviour {
    
    Button pauseButton;
    [HideInInspector]
    public GameManager gmScript;
    [HideInInspector]
    public GameObject pauseMenu;
    
    public GameObject restartButton_GO;
    
    public GameObject backButton_GO;
    Button restartButton;
    Button backButton;

	// Use this for initialization
	void Start () {

        pauseButton = GetComponent<Button>();
        pauseButton.onClick.AddListener(() => PauseButtonTapped(pauseButton));

        restartButton = restartButton_GO.GetComponent<Button>();
        restartButton.onClick.AddListener(() => RestartButtonTapped(restartButton));

        backButton = backButton_GO.GetComponent<Button>();
        backButton.onClick.AddListener(() => BackButtonTapped(backButton));

    }
    

    void PauseButtonTapped(Button bt)
    {
        
        gmScript.globalGameStart = false;
        pauseMenu.SetActive(true);
        Screen.sleepTimeout = SleepTimeout.SystemSetting;


    }

    void RestartButtonTapped(Button bt)
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    void BackButtonTapped(Button bt)
    {
        pauseMenu.SetActive(false);
        gmScript.globalGameStart = true;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }
}
