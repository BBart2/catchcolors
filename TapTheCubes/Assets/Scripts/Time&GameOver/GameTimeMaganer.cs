﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameTimeMaganer : MonoBehaviour {

    public static float time;
    public GameManager GamManagScript;
    //GameOverManager gameOverMgScript;
    bool bonusTime = false;
    bool panelSpawned = false;

    float m_RemainingTimeSeconds = 0;
    float m_MaxLimiteTimeSeconds = 60;
    public Text m_Gameplay_Text_RemainingTime;
    public Slider m_Gameplay_Slider_Clock;

    Text text;


    void Awake()
    {
        m_RemainingTimeSeconds = m_MaxLimiteTimeSeconds;
        text = GetComponent<Text>();
        time = 60f;
        
    }


    void Update()
    {
        bonusTime = GameManager.b_bonusTIME;
        if (time >= 0 && !bonusTime && (GamManagScript.globalGameStart))
        {
            UpdateTime();
            //time -= Time.deltaTime;
            //text.text = ((int)time).ToString();
        }
        
    }


    void UpdateTime()
    {
        time -= Time.deltaTime;
        m_Gameplay_Text_RemainingTime.text = string.Format(string.Format("{00:00}", (int)time % 60));
        float value_RemainingTime = (float)time / m_MaxLimiteTimeSeconds;
        m_Gameplay_Slider_Clock.value = value_RemainingTime;
    }
}