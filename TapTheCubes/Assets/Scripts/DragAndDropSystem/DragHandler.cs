﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class DragHandler : MonoBehaviour, IBeginDragHandler , IDragHandler , IEndDragHandler
{
    public static GameObject itemBeingDragged;
    Vector3 startPosition;
    Transform startParent;
   // Transform tempParent;  // for MASK

    //void Start()
    //{
    //    //tempParent = GameObject.FindGameObjectWithTag("TempMaskParent").transform;  // MASK
    //}

    public void OnBeginDrag(PointerEventData eventData)
    {
        itemBeingDragged = gameObject;
        startPosition = transform.position;
        startParent = transform.parent;
        //transform.parent.SetParent(tempParent); 
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        itemBeingDragged = null;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
       // if (transform.parent == startParent)
       // {
            transform.parent = startParent;
            transform.position = startPosition;
        //}
    }
}
