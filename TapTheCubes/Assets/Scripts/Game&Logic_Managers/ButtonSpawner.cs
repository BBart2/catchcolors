﻿//using UnityEngine;
//using UnityEngine.UI;
//using System.Collections;

//public class ButtonSpawner : MonoBehaviour
//{
//    public GeneratePattern GenPattScript;
//    ScoreManager scoreMgScript;

//    public GameObject basicButton;
//    public GameObject StartPos;
//   // public GameObject parrentCanvas;
//    public GameObject[] buttonArr = new GameObject[16];
//    public Color[] buttonsColors = new Color[7] { Color.green, Color.blue, Color.yellow, Color.cyan, Color.gray, Color.grey, Color.white };
//    public GameObject[] patternArr = new GameObject[4];

//    public LifeManagerV2 lifeManagerScript;

//    Vector2 posOfButton; //= new Vector2(-780,6);
//    GameObject emptyButton;
//    GameObject button;
//    GameObject choseRandomButton;
//    Color basicButtonColor;
//    float changeButtonTimer = 0f;
//    int lifeNumber;
//    int succesfullMatch = 0;
//    bool gameOver = false;




//    // Use this for initialization
//    void Start()
//    {
//        scoreMgScript = GameObject.FindGameObjectWithTag("ScoreManager").GetComponent<ScoreManager>();
//        InitializateThePatternArray(patternArr, GenPattScript.imagePatternArr);
//        basicButtonColor = basicButton.GetComponent<Image>().color;
//        EmptyButtonInitialization();
//        // ColorInitialization(buttonsColors);
//        lifeNumber = LifeManagerV2.lifeNumber;


//    }

//    // Update is called once per frame
//    void Update()
//    {
//        changeButtonTimer += Time.deltaTime;

//        if (GameTimeMaganer.time <= 0)
//        {
//            gameOver = true;
//        }

//        if (changeButtonTimer > 0.4f && !gameOver)
//        {
//            GameLogic();
//        }

//    }



//    void GameLogic()
//    {
//        changeButtonTimer = 0f;
//        Color currentColor;
//        choseRandomButton = ChooseRandomButton(buttonArr);
//        currentColor = ChooseRandomColor(buttonsColors);
//        while (choseRandomButton.GetComponent<Image>().color == currentColor) { currentColor = ChooseRandomColor(buttonsColors); }
//        ChangeColorOfButton(choseRandomButton, currentColor);
//    }

//    GameObject ChooseRandomButton(GameObject[] arr)
//    {
//        int indexI = Random.Range(0, 16);
//        button = buttonArr[indexI];
//        return button;
//    }

//    public Color ChooseRandomColor(Color[] c)
//    {
//        int index = Random.Range(0, c.Length);
//        return c[index];
//    }

//    void ChangeColorOfButton(GameObject button, Color c)
//    {
//        button.GetComponent<Image>().color = c;
//    }

//    void EmptyButtonInitialization()
//    {
//        int index = 0;
//        Color randomColor;
//        float startXpos = StartPos.transform.position.x - 50;
//        float startYpos = basicButton.transform.position.y +50;
//        posOfButton = new Vector2(startXpos, startYpos);
//        emptyButton = basicButton;


//        for (int i = 0; i < 4; i++)
//        {
//            posOfButton = new Vector2(startXpos, posOfButton.y + 100);
			
//            for (int j = 0; j < 4; j++)
//            {
//                posOfButton = new Vector2(posOfButton.x + 100, posOfButton.y);
//                button = (GameObject)Instantiate(emptyButton, posOfButton, Quaternion.identity);
//                button.transform.parent = StartPos.transform; // parentCanvas

//				RectTransform rectTransform = button.GetComponent<RectTransform>();

//				//float x = -containerRectTransform.rect.width / 2 ;
//				//float y = containerRectTransform.rect.height / 2  ;
//				//rectTransform.offsetMin = new Vector2(x, y);
				
//				//x = rectTransform.offsetMin.x + width;
//				//y = rectTransform.offsetMin.y + height;
//				//rectTransform.offsetMax = new Vector2(x, y);

//                //button.transform.localScale *= 0.5f;
//                button.tag = "EmptyButton";
//                button.name += index;

//                randomColor = ChooseRandomColor(buttonsColors);
//                button.GetComponent<Image>().color = randomColor;
//                Button tempButton = button.GetComponent<Button>();
//                tempButton.onClick.AddListener(() => onClickButton(tempButton));

//                buttonArr[index] = button;
//                index++;
//            }
//        }
//    }

//    void InitializateThePatternArray(GameObject[] destination, GameObject[] source)
//    {
//        // int lenght = source.Length;
//        //destination = new GameObject[lenght];
//        for (int j = 0; j < 4; j++)
//        {
//            destination[j] = source[j];
//        }


//    }

//    void onClickButton(Button butt)
//    {
//        if (!gameOver)
//        {
//            Color c;
//            c = butt.GetComponent<Image>().color;
//            if (c == patternArr[0].GetComponent<Image>().color)
//                //Debug.Log(c);
//                OnSuccessMatch(butt, patternArr);
//            else
//                BadMatch();
//        }
//    }

//    void OnSuccessMatch(Button matchButton, GameObject[] matchPattern)
//    {
//        int index = 0;
//        matchButton.GetComponent<Image>().color = ChooseRandomColor(buttonsColors); //basicButtonColor;  // ustawienie basic coloru na tapniety button
//        for (index = 0; index < 3; index++)
//        {
//            matchPattern[index].GetComponent<Image>().color = matchPattern[index + 1].GetComponent<Image>().color;
//        }
//        if (index + 1 == matchPattern.Length)
//        {
//            matchPattern[index].GetComponent<Image>().color = ChooseRandomColor(buttonsColors);
//        }
//        ScoreManager.score += 1;
//        succesfullMatch++;
//        if (succesfullMatch % 5 == 0)
//        {
//            GameTimeMaganer.time += 3f;
//        }
//    }

//    void BadMatch()
//    {
//        //lifeNumber--;
//        //lifeManagerScript.DecressLife(lifeNumber);
//        succesfullMatch = 0;
//        if (GameTimeMaganer.time > 10)
//        {
//            GameTimeMaganer.time -= 10f;
//        }
//        else
//            GameTimeMaganer.time = 0f;
//        // if(lifeNumber <= 0)
//        //{
//        //    gameOver = true;
//        //    Debug.Log("GameOver!");
//        //}
//    }

//}
